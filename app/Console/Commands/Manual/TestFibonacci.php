<?php
namespace App\Console\Commands\Manual;

use App\Jobs\AccrueDeposit;
use App\Jobs\CloseDeposit;
use App\Jobs\HandleRankJob;
use App\Models\Currency;
use App\Models\Deposit;
use App\Models\ExchangeOrder;
use App\Models\Licences;
use App\Models\OrderRequest;
use App\Models\Rate;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use App\Modules\PaymentSystems\EtherApiModule;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class TestFibonacci
 * @package App\Console\Commands\Manual
 */
class TestFibonacci extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:fibo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check all users balances.';

    /** @var string $userId */
    protected $userId;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = ExchangeOrder::query()
            ->where('main_currency_id', Currency::getByCode('WEC')->id)
            ->where('currency_id', Currency::getByCode('USD')->id)
            ->where('active', 1)->get();
        /**
         * @var ExchangeOrder $order
         */
        foreach ($orders as $order) {
            $order->close();
        }

    }





}
