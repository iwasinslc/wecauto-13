<?php
namespace App\Console;

use App\Console\Commands\Automatic\ArchiveBlockioAddressesCommand;
use App\Console\Commands\Automatic\CheckEthereumConfirmationsCommand;
use App\Console\Commands\Automatic\CheckPaymentSystemsConnectionsCommand;
use App\Console\Commands\Automatic\CleanAfterDeploymentCommand;
use App\Console\Commands\Automatic\CleanDemoCommand;
use App\Console\Commands\Automatic\CleanSentMailsCommand;
use App\Console\Commands\Automatic\DepositQueueCommand;
use App\Console\Commands\Automatic\FillCacheCommand;
use App\Console\Commands\Automatic\GenerateDemoDataCommand;
use App\Console\Commands\Automatic\ProcessInstantPaymentsCommand;
use App\Console\Commands\Automatic\PublishLanguageFilesCommand;
use App\Console\Commands\Automatic\ScanSysLoadCommand;
use App\Console\Commands\Automatic\ScriptCheckerCommand;
use App\Console\Commands\Automatic\StashOrdersCommand;
use App\Console\Commands\Automatic\TaskCheck\CheckAllScopes;
use App\Console\Commands\Automatic\TaskCheck\CleanTasksWithoutActions;
use App\Console\Commands\Automatic\TaskCheck\Facebook\FacebookNewFriendsCommand;
use App\Console\Commands\Automatic\TaskCheck\Facebook\FacebookPageLikeCommand;
use App\Console\Commands\Automatic\TaskCheck\Telegram\TelegramChannelSubscriptionCommand;
use App\Console\Commands\Automatic\TaskCheck\VK\VkPageSubscriptionCommand;
use App\Console\Commands\Automatic\TaskCheck\VK\VkPostLikeCommand;
use App\Console\Commands\Automatic\TaskCheck\Youtube\YoutubeChannelSubscriptionCommand;
use App\Console\Commands\Automatic\TaskCheck\Youtube\YoutubeVideoCommentCommand;
use App\Console\Commands\Automatic\TaskCheck\Youtube\YoutubeVideoLikeCommand;
use App\Console\Commands\Automatic\TaskCheck\Youtube\YoutubeVideoWatchCommand;
use App\Console\Commands\Automatic\Telegram\ClearTelegramBotHistoryCommand;
use App\Console\Commands\Automatic\Telegram\UpdateWebhookInfoCommand;
use App\Console\Commands\Automatic\UpdateCurrencyRatesCommand;
use App\Console\Commands\Manual\CheckEthereumBalancesCommand;
use App\Console\Commands\Manual\CheckUsersBalancesCommand;
use App\Console\Commands\Manual\CreateRootCommand;
use App\Console\Commands\Manual\InstallScriptCommand;
use App\Console\Commands\Manual\RegisterCurrenciesCommand;
use App\Console\Commands\Manual\RegisterPaymentSystemsCommand;
use App\Console\Commands\Manual\RegisterWalletCommand;
use App\Console\Commands\Manual\Telegram\TelegramDeleteBotCommand;
use App\Console\Commands\Manual\Telegram\TelegramRegisterBotCommand;
use App\Console\Commands\Manual\Telegram\TelegramSetWebhookCommand;
use App\Console\Commands\Manual\UnarchiveAllBlockioAddressesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        /*
         * Each different
         */
        CleanAfterDeploymentCommand::class,
        CleanDemoCommand::class,
        GenerateDemoDataCommand::class,
        CreateRootCommand::class,
        InstallScriptCommand::class,
        RegisterCurrenciesCommand::class,
        RegisterPaymentSystemsCommand::class,
        ProcessInstantPaymentsCommand::class,
        PublishLanguageFilesCommand::class,
        CheckPaymentSystemsConnectionsCommand::class,
        ArchiveBlockioAddressesCommand::class,
        UnarchiveAllBlockioAddressesCommand::class,
        ScriptCheckerCommand::class,
        TelegramRegisterBotCommand::class,
        TelegramDeleteBotCommand::class,
        TelegramSetWebhookCommand::class,
        ClearTelegramBotHistoryCommand::class,
        UpdateWebhookInfoCommand::class,
        ScanSysLoadCommand::class,
        DepositQueueCommand::class,
        CheckUsersBalancesCommand::class,
        CleanSentMailsCommand::class,
        FillCacheCommand::class,
        UpdateCurrencyRatesCommand::class,
        CheckEthereumConfirmationsCommand::class,
        CheckEthereumBalancesCommand::class,
        RegisterWalletCommand::class,

        /*
         * Task scopes
         */
        FacebookNewFriendsCommand::class,
        FacebookPageLikeCommand::class,

        TelegramChannelSubscriptionCommand::class,

        VkPageSubscriptionCommand::class,
        VkPostLikeCommand::class,

        YoutubeChannelSubscriptionCommand::class,
        YoutubeVideoCommentCommand::class,
        YoutubeVideoLikeCommand::class,
        YoutubeVideoWatchCommand::class,

        CheckAllScopes::class,
        CleanTasksWithoutActions::class,
        StashOrdersCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Logs
        $schedule->command('log:clear')->daily()->withoutOverlapping();

        // Old information
        $schedule->command('telegram:clear_history')->everyTenMinutes()->withoutOverlapping();
        $schedule->command('clean:sent_mails')->everyTenMinutes()->withoutOverlapping();

        // Demo
        $schedule->command('clean:demo')->daily()->at('03:00');

        // Jobs
        $schedule->command('horizon:snapshot')->everyFiveMinutes();

        $schedule->command('deposits:queue')->withoutOverlapping()->hourly();
        // Financial
        $schedule->command('check:payment_systems_connections')->everyTenMinutes()->withoutOverlapping();
        $schedule->command('process:instant_payments')->everyFiveMinutes()->withoutOverlapping();
        //$schedule->command('check:not_payed')->hourly()->withoutOverlapping();
        //$schedule->command('rate:change')->daily()->at('01:00')->withoutOverlapping();
        $schedule->command('stash:orders')->daily()->at('12:00')->withoutOverlapping();
        //$schedule->command('rate:statistic')->hourly()->withoutOverlapping();
        // Languages
        $schedule->command('publish:language_files')->everyTenMinutes()->withoutOverlapping();


        $schedule->command('update:currency_rates')->hourly()->withoutOverlapping();

        // server
        $schedule->command('check:script')->everyTenMinutes()->withoutOverlapping();
        //$schedule->command('scan:sys_load')->everyMinute()->withoutOverlapping();
        $schedule->command('clean:page_views')->everyFifteenMinutes()->withoutOverlapping();

        // Update webhook info
        //$schedule->command('telegram:update_webhook_info')->everyMinute()->withoutOverlapping();

        // CACHE
        //$schedule->command('fill:cache')->hourly()->withoutOverlapping();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands/Automatic');
        $this->load(__DIR__ . '/Commands/Manual');

        require base_path('routes/console.php');
    }
}
