<?php

/**
 * Created by PhpStorm.
 * User: Arkai
 * Date: 18.12.2018
 * Time: 17:22
 */

namespace App\Exports\Admin;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Deposit;

class RanksExport implements FromCollection, WithHeadings
{
    use Exportable;


    public function __construct()
    {

    }

    public function collection()
    {
        /**
         * @var User $user
         */
        $type  = TransactionType::getByName('set_rank');
        $transactions = Transaction::with(['rank', 'user'])->where('type_id', $type->id)->orderBy('created_at', 'desc')->get()->map(function ($transaction) {
            return [
                'rank'=>$transaction->rank->name,
                'user'=>$transaction->user->login,
                'created_at'=>$transaction->created_at->format('d-m-Y H:i'),
            ];
        });
        /**
         * @var Transaction $transaction
         */

        return $transactions;
    }

    public function headings(): array
    {
        return [
            'Rank',
            'Login',
            'Date'
        ];
    }
}