<?php
use Twilio\Rest\Client;

/**
 * @param $number
 * @param $text
 * @return bool|\Twilio\Rest\Api\V2010\Account\MessageInstance
 * @throws \Twilio\Exceptions\ConfigurationException
 *
 * TODO: twilio have to be as module
 */
function sendSmsTwilio($number, $text)
{
    // Your Account SID and Auth Token from twilio.com/console
    $sid = config('sms.set_twilio_sid');
    $token = config('sms.set_twilio_token');
    $client = new Client($sid, $token);
    $purchasedTwilioNumber = config('sms.set_twilio_number');

    try {
        return $client->messages->create(
            $number,
            [
                // A Twilio phone number you purchased at twilio.com/console
                'from' => $purchasedTwilioNumber,
                'body' => $text
            ]
        );
    } catch(Exception $e) {
        return false;
    }
}


function limit_iteration($price)
{
    if ($price<0.2) return 100;


    $current = 0.1;
    $value = 100;
    $minus = 5;



    while (true)
    {

        $current = $current*2;

        if ($price<$current)
        {

            if ($value<=50)
            {
                return 50;
            }
            return $value;
        }

        $value = $value-$minus;

    }

    return 100;


}



function link_it($text)
{
    $text= preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);
    $text= preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http://$3\" >$3</a>", $text);
    $text= preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);
    return($text);
}

/**
 * @param string $currencyId
 * @param float $value
 * @return float
 * @throws Exception
 */
function currencyPrecision(string $currencyId, float $value): float
{
    $precision = cache()->tags(['currency', 'precision'])->rememberForever('precision_' . $currencyId, function () use ($currencyId) {
        /** @var \App\Models\Currency $currency */
        $currency = \App\Models\Currency::find($currencyId);

        return $currency->precision > 0
            ? $currency->precision
            : 2;
    });
    return round($value, $precision);
}

/**
 * @param array $keys
 * @return void
 * @throws Exception
 */
function clearCacheByArray(array $keys)
{
    foreach ($keys as $key) {
        cache()->forget($key);
    }
}

/**
 * @param array $tags
 * @return void
 * @throws Exception
 */
function clearCacheByTags(array $tags)
{
    cache()->tags($tags)->flush();
}

/**
 * @return array
 * @throws Exception
 */
function getTransactionTypes(): array
{
    return cache()->remember('h.transactionTypes', getCacheHLifetime('transactionTypes'), function() {
        return \App\Models\TransactionType::get()->toArray();
    });
}

/**
 * @return int
 */
function generateMyId($addNumber=1): int
{

    $maxExists = rand(500000, 2000000);

    $checkExists = \App\Models\User::where('my_id', $maxExists)->count();

    if ($checkExists > 0) {
        $maxExists = generateMyId($addNumber+1);
    }

    \Log::info('new my_id '.$maxExists);

    return $maxExists;
}

/**
 * @param string $baseCurrency
 * @return array
 * @throws Exception
 *
 * TODO: currency rates have to be as module
 */
function currenciesRates(string $baseCurrency = 'USD'): array
{
    return cache()->tags(['currency', 'precision', 'rates'])->remember('rates_' . $baseCurrency, getCacheHLifetime('currenciesRates'), function () use ($baseCurrency) {
        $rates = \App\Models\Currency::balances();
        array_pull($rates, $baseCurrency);
        $keys = implode(",", array_keys($rates));

        try {
            $f = fopen('https://openexchangerates.org/api/latest.json?app_id=' . env('OPENEXCHANGERATES_API') . '&base=' . $baseCurrency . '&symbols=' . $keys . '&show_alternative=true', 'rb');

            if ($f) {
                $out = "";

                while (!feof($f)) {
                    $out .= fgets($f);
                }

                fclose($f);
                $out = json_decode($out, true);

                if (isset($out['rates'])) {
                    foreach ($out['rates'] as $key => $value) {
                        $rates[$key] = currencyPrecision($baseCurrency, (1 / $value));
                    }
                }
            }
        } finally {
            return $rates;
        }
    });
}

/**
 * @param string|null $key
 * @param string|null $section
 * @return \Carbon\Carbon
 * @throws Exception
 */
function getCacheLifetime($key = null, $section = null)
{
    if (null == $key) {
        throw new Exception('Cache key is empty');
    }

    if (null == $section) {
        throw new Exception('Cache section is empty');
    }

    return now()->addMinutes(config()->get('cache.lifetimes.' . $section . '.' . $key));
}

/**
 * @param null $key
 * @return \Carbon\Carbon
 * @throws Exception
 */
function getCacheILifetime($key = null)
{
    return getCacheLifetime($key, 'i');
}

/**
 * @param null $key
 * @return \Carbon\Carbon
 * @throws Exception
 */
function getCacheALifetime($key = null)
{
    return getCacheLifetime($key, 'a');
}

/**
 * @param null $key
 * @return \Carbon\Carbon
 * @throws Exception
 */
function getCacheHLifetime($key = null)
{
    return getCacheLifetime($key, 'h');
}

/**
 * @param null $key
 * @return \Carbon\Carbon
 * @throws Exception
 */
function getCacheCLifetime($key = null)
{
    return getCacheLifetime($key, 'c');
}

/**
 * @return string
 */
function getSupervisorName()
{
    return preg_replace('/ /', '-', env('APP_NAME', 'supervisor-1'));
}

/**
 * @param \App\Models\Transaction $enterTransaction
 * @return bool
 * @throws Exception
 */
function autocreatedeposit(\App\Models\Transaction $enterTransaction)
{
    $user = $enterTransaction->user;
    $wallet = $enterTransaction->wallet;
    $amount = $enterTransaction->amount;

    if (null === $user || null === $wallet) {
        return false;
    }

    $lookForAutoCreateRecord = \App\Models\AutoCreateDeposit::where('user_id', $user->id)
        ->where('wallet_id', $wallet->id)
        ->where('amount', $amount)
        ->orderBy('created_at', 'desc')
        ->first();

    if (null === $lookForAutoCreateRecord) {
        return false;
    }

    $depositData = [
        'wallet_id'  => $wallet->id,
        'rate_id'    => $lookForAutoCreateRecord->rate_id,
        'amount'     => $amount,
        'reinvest'   => 0,
        'created_at' => now(),
        'user'       => $user,
    ];
    $depo = \App\Models\Deposit::addDeposit($depositData);

    if (null === $depo) {
        return false;
    }

    $lookForAutoCreateRecord->delete();

    return true;
}