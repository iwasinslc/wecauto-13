<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PageViews;

/**
 * Class PageViewsController
 * @package App\Http\Controllers\Admin
 */
class PageViewsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $pageViews = PageViews::find($id);

        return view('admin.page_views.show', [
            'pageViews' => $pageViews,
        ]);
    }
}
