<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RequestDataTable;
use App\Http\Resources\TradeResource;
use App\Models\OrderPiece;

class TradesController extends Controller
{
    /**
     * @OA\Get(
     *      path="/trades",
     *      operationId="getTrades",
     *      tags={"Trades"},
     *      summary="Get list of the trades",
     *      description="Return array of the trades. Filters: user_id",
     *      @OA\Parameter(
     *          ref="#/components/parameters/user_id"
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/TradeResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function index(RequestDataTable $request) {
        return TradeResource::collection(
                OrderPiece::with(['currency', 'mainCurrency'])
                    ->where('user_id', $request->user_id)
                    ->get()
            );
    }

}