<?php
namespace App\Http\Controllers\Profile;

use App\Events\ExchangeAllOperations;
use App\Events\Message;
use App\Http\Controllers\Controller;
use \App\Http\Requests\Exchange\RequestExchange;

use App\Http\Requests\Exchange\RequestExchangeCommon;
use App\Models\Currency;
use App\Models\ExchangeOrder;
use App\Models\OrderPiece;
use App\Models\OrderRequest;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\Wallet;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

/**
 * Class ExchangeController
 * @package App\Http\Controllers\Profile
 */
class ExchangeController extends Controller
{
    public function index()
    {
        $code='WEC';
        $mainCode = 'FST';

        return view('profile.exchange', [
            'code'=>$code,
            'mainCode'=>$mainCode
        ]);
    }



    public function currency($mainCode = 'FST', $code='WEC')
    {
        if (!in_array($code, ['WEC', 'ACC']))
        {
            return back()->with('error', __('Error'));
        }

        if (!in_array($mainCode, ['FST']))
        {
            return back()->with('error', __('Error'));
        }



        return view('profile.exchange', [

            'code'=>$code,
            'mainCode'=>$mainCode
        ]);
    }

    public function wec_exchange($mainCode = 'WEC', $code='USD')
    {

//
//        if (!\user()->hasRole('root'))
//        {
//            return back()->with('error', __('Error'));
//        }

        return view('profile.exchange', [

            'code'=>$code,
            'mainCode'=>$mainCode
        ]);
    }



    public function buy_orders($mainCode, $code)
    {
        $orders = cache()->tags('OrdersTableCommon')->remember('OrdersTableCommon.Buy.'.$code.$mainCode,  now()->addMinutes(10), function () use ($code, $mainCode)   {
            return ExchangeOrder::query()->select(DB::raw('rate, sum(amount) as sum'))
                ->where('type', ExchangeOrder::TYPE_BUY)
                ->where('currency_id', Currency::getByCode($code)->id)
                ->where('main_currency_id', Currency::getByCode($mainCode)->id)
                ->active()
                ->groupBy('rate')
                ->orderBy('rate', 'desc')
                ->get();
        });

        return DataTables::of($orders)
            ->addColumn('num_rate', function (ExchangeOrder $order) {

                return number_format($order->rate, 8, '.' ,'');
            })
            ->make(true);
    }


    public function sell_orders($mainCode, $code)
    {
        $orders = cache()->tags('OrdersTableCommon')->remember('OrdersTableCommon.Sell.'.$code.$mainCode,  now()->addMinutes(10), function () use ($code, $mainCode)   {
            return ExchangeOrder::query()->select(DB::raw('rate, sum(amount) as sum'))->active()

                ->where('currency_id', Currency::getByCode($code)->id)
                ->where('main_currency_id', Currency::getByCode($mainCode)->id)
                ->where('type', ExchangeOrder::TYPE_SELL)
                ->groupBy('rate')
                ->orderBy('rate', 'asc')
                ->get();
        });

        return DataTables::of($orders)
            ->addColumn('num_rate', function (ExchangeOrder $order) {

                return number_format($order->rate, 8, '.' ,'');
            })
            ->make(true);
    }



    public function my_orders($mainCode = 'FST', $code='WEC')
    {


        $myOrders = cache()->tags('MyOrders')->remember('MyOrders.'.user()->id.$code.$mainCode,  now()->addMinutes(10), function () use ($code, $mainCode)    {
            return user()->exchangeOrders()
                ->where('currency_id', Currency::getByCode($code)->id)
                ->where('main_currency_id', Currency::getByCode($mainCode)->id)
                ->with(['currency', 'mainCurrency'])
                ->orderBy('created_at', 'desc')
                ->get();
        });

        return DataTables::of($myOrders)
            ->addColumn('new_amount', function (ExchangeOrder $order) {

                return number_format($order->amount, $order->mainCurrency->precision);
            })
            ->addColumn('value', function (ExchangeOrder $order) {

                return number_format($order->amount*$order->rate, $order->mainCurrency->precision);
            })
            ->make(true);
    }






    /**
     * @param RequestExchangeCommon $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exchange(RequestExchangeCommon $request)
    {



//        if (!user()->hasRole(['root']))
//        {
//            return back()->with('error', __('Function dont working'));
//        }

        $data = cache()->pull('protect-exchange-'.getUserId());

        if ($data!==null)
        {

            return back()->with('error', __('Error'));
        }




        cache()->put('protect-exchange-'.getUserId(), '1', now()->addSeconds(10));

        $user = user();



        /**
         * @var Wallet $main_wallet
         */
        $main_wallet = $user->wallets()->find($request->main_wallet_id);
        /**
         * @var Wallet $wallet
         */
        $wallet = $user->wallets()->find($request->wallet_id);


        if ($request->type==ExchangeOrder::TYPE_SELL&&$main_wallet->currency->code=='WEC')
        {
            if (!$user->deposits()->where('active', false)->exists())
            {
                return back()->with('error', __('У вас нет закрытого депозита!'));
            }
        }

//        if ($main_wallet->currency->code=='ACC'&&$wallet->currency->code=='WEC')
//        {
//                return back()->with('error', __('Sell ACC Closed'));
//        }

        OrderRequest::addRequest($main_wallet, $wallet, $request->amount, $request->type, round($request->rate, 8) );

        return back()->with('success', __('Request created successful'));
    }


    /**
     * @param  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function close_order(Request $request, $id)
    {

        /**
         * @var ExchangeOrder $order
         */


        $data = cache()->pull('protect-exchange-'.getUserId());

        if ($data!==null)
        {

            return back()->with('error', __('Error'));
        }

        cache()->put('protect-exchange-'.getUserId(), '1', 5);



        $order = user()->exchangeOrders()->where('id', $id)->where('active',1)->first();



        if ($order===null)
        {
            return back()->with('error', __('Order not found!'));
        }

        $order->close();

        return back()->with('success', __('Order closed successful!'));

    }


    public function dataTableExchange($mainCode, $code)
    {

        $operations = cache()->tags('userExchangePieces.' . getUserId())->remember('c.' . getUserId() . '.userExchangePieces.'.$code.$mainCode, now()->addMinutes(10), function () use ($code, $mainCode)  {
            $operations = OrderPiece::where('user_id', getUserId())->with([ 'currency', 'mainCurrency'])
                ->where('currency_id', Currency::getByCode($code)->id)
                ->where('main_currency_id', Currency::getByCode($mainCode)->id)
                ->get();

            return $operations;
        });

        return DataTables::of($operations)
            ->make(true);
    }


    public function dataTableExchangeAll($mainCode, $code)
    {
        $operations = cache()->tags('exchangePieces')->remember('c.exchangePieces.'.$code.$mainCode , now()->addMinutes(10), function () use ($code, $mainCode)  {

            $operations = OrderPiece::query();


            return $operations
                ->with( 'currency', 'mainCurrency')
                ->where('currency_id', Currency::getByCode($code)->id)
                ->where('main_currency_id', Currency::getByCode($mainCode)->id)
                ->orderBy('created_at', 'desc')
                ->limit(100)
                ->get();
        });

        return DataTables::of($operations)
            ->make(true);
    }




}
