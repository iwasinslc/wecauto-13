<?php

namespace App\Jobs;

use App\Modules\CyberwecModule;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CyberwecRegisterNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /** @var string $cyberId */
    protected $cyberId;

    /** @var string $partnerId */
    protected $partnerId;

    /**
     * Create a new job instance.
     *
     * @param string $cyberId
     * @param string|null $partnerId
     * @return void
     */
    public function __construct(string $cyberId, string $partnerId = null)
    {
        $this->cyberId = $cyberId;
        $this->partnerId = $partnerId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = [
            'cyberwec_id'   => $this->cyberId,
            'project_id' => $this->partnerId,
        ];

        $cyberwec = new CyberwecModule();

        $cyberwec->registerCallback($data);
    }
}
