<?php
namespace App\Models;

use App\Traits\ModelTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRank
 * @package App\Models
 *
 * @property string name
 * @property string confines
 * @property float daily_limit
 * @property integer levels
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class UserRank extends Model
{
    use ModelTrait;

    /** @var array $fillable */
    protected $fillable = [
        'name',
        'confines',
        'levels',
        'daily_limit',
        'created_at'
    ];


}
