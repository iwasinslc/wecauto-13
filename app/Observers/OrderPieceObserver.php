<?php
namespace App\Observers;

use App\Models\Currency;
use App\Models\ExchangeOrder;
use App\Models\OrderPiece;

/**
 * Class CurrencyObserver
 * @package App\Observers
 */
class OrderPieceObserver
{
    /**
     * @param Currency $currency
     */
    public function deleting(OrderPiece $order)
    {

    }

    /**
     * Listen to the Currency created event.
     *
     * @param Currency $currency
     * @return void
     * @throws
     */
    public function created(OrderPiece $order)
    {
        clearCacheByArray($this->getCacheKeys($order));
        clearCacheByTags($this->getCacheTags($order));
    }

    /**
     * @param ExchangeOrder $order
     * @return array
     */
    private function getCacheKeys(OrderPiece $order): array
    {
        return [
            'sellLimit.'.$order->user_id,
            'buyLimit.'.$order->user_id
        ];
    }

    /**
     * @param Currency $currency
     * @return array
     */
    private function getCacheTags(OrderPiece $order): array
    {
        return [
            'OrdersTable',
            'OrdersTableCommon',
            'MyOrders',
            'MyOrdersBalance',
            'exchangePieces',
            'userExchangePieces.'.$order->user_id,
        ];
    }

    /**
     * Listen to the Currency deleting event.
     *
     * @param Currency $currency
     * @return void
     * @throws
     */
    public function deleted(OrderPiece $order)
    {
        clearCacheByArray($this->getCacheKeys($order));
        clearCacheByTags($this->getCacheTags($order));
    }

    /**
     * Listen to the Currency updating event.
     *
     * @param Currency $currency
     * @return void
     * @throws
     */
    public function updated(OrderPiece $order)
    {
        clearCacheByArray($this->getCacheKeys($order));
        clearCacheByTags($this->getCacheTags($order));
    }
}