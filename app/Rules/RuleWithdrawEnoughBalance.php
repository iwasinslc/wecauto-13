<?php
namespace App\Rules;

use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RuleEnoughBalance
 * @package App\Rules
 */
class RuleWithdrawEnoughBalance implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $extractCurrency = explode(':', request()->currency);

        if (count($extractCurrency) != 2) {
            return back()->with('error', __('Unable to read data from request'))->withInput();
        }
        $paymentSystem = PaymentSystem::where('id', $extractCurrency[1])->first();

        $currency = $paymentSystem->currencies()->where('id', $extractCurrency[0])->first();

        /**
         * @var Wallet $wallet
         */
        $wallet = user()->wallets()->where('currency_id', $currency->id)->where('payment_system_id', $paymentSystem->id)->first();


        if (!in_array($wallet->currency->code, ['WEC', 'BIP', 'PZM', 'ACC']))
        {
            $wallet = user()
                ->wallets()
                ->where('payment_system_id', PaymentSystem::getByCode('perfectmoney')->id)
                ->where('currency_id', Currency::getByCode('USD')->id)
                ->first();

        }

        $amount = request()->amount;

//        if ($wallet->currency->code=='WEC')
//        {
//            $commission = 0.1;
//        }

        return $wallet ? $wallet->balance >= $amount : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.enough_balance');
    }
}
