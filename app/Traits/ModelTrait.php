<?php


namespace App\Traits;

/**
 * General model trait.
 * In this trait general settings for all models can be set.
 * Trait ModelTrait
 * @package App\Traits
 */
trait ModelTrait
{
    /**
     * Set serialized date format for methods toArray and toJson
     * @param \DateTimeInterface $date
     * @return string
     */
    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}