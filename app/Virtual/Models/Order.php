<?php


namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Order",
 *     description="Order information"
 * )
 */
class Order
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="Order ID",
     *     example="b5d00f20-ff1d-11ea-8e8c-211d59c5f58c"
     * )
     *
     * @var string
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Type",
     *     description="Order type",
     *     example="sell"
     * )
     *
     * @var string
     */
    private $type;

    /**
     * @OA\Property(
     *     title="Value",
     *     description="Order amount",
     *     example=1000.00
     * )
     *
     * @var float
     */

    public $value;

    /**
     * @OA\Property(
     *     title="Currency",
     *     description="Currency code of the order amount",
     *     example="WEC"
     * )
     *
     * @var string
     */
    public $currency;

    /**
     * @OA\Property(
     *     title="Rate",
     *     description="Rate for current order",
     *     example=1000.00
     * )
     *
     * @var float
     */

    public $rate;

    /**
     * @OA\Property(
     *     title="Rate Currency",
     *     description="Currency code of rate",
     *     example="WEC"
     * )
     *
     * @var string
     */
    public $rate_currency;

    /**
     * @OA\Property(
     *     title="Rate amount",
     *     description="Amont converted to rate",
     *     example=1000.00
     * )
     *
     * @var float
     */

    public $rate_amount;

    /**
     * @OA\Property(
     *     title="License rate amount",
     *     description="Rate for license",
     *     example=1000.00
     * )
     *
     * @var float
     */
    public $licence_rate;

    /**
     * @OA\Property(
     *     title="License Currency",
     *     description="Currency code of license",
     *     example="USD"
     * )
     *
     * @var string
     */
    public $licence_currency;

    /**
     * @OA\Property(
     *     title="Status",
     *     description="Show status of order",
     *     example="string"
     * )
     *
     * @var boolean
     */
    public $status;

    /**
     * @OA\Property(
     *     title="Created At",
     *     description="Date of the creating transaction",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $created_at;

    /**
     * @OA\Property(
     *     title="Updated At",
     *     description="Date of the updating order",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $updated_at;
}