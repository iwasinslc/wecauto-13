<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            LanguagesTableSeeder::class,
            LicencesSeeder::class,
            RolesAndPermissionsSeeder::class,
            TransactionTypesSeeder::class,
            TransactionStatusesSeeder::class,
            CountriesSeeder::class,
            SettingSeeder::class,
            TranslationsSeeder::class,
            TelegramBotSeeder::class,
            TaskScopesSeeder::class,
            PricesSeeder::class,
            UserRanksSeeder::class
        ]);
    }
}
