@extends('layouts.customer')
@section('title', __('WEC Auto'))


@section('top_content')
    <div class="intro__slider swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="intro-slide">
                    <div class="container">
                        <div class="intro-slide__row">
                            <div class="intro-slide__col">
                                <div class="intro-slide__desc">
                                    <p>BMW X5, 3.0D, 2020</p>
                                    <p><strong>{!! __('for') !!} 58,900 USD</strong></p>
                                </div>
                                <h3 class="intro-slide__title">
                                    <big>{!! __('Pay for') !!} 30% </big><span>{!! __('Get your car!') !!}</span>
                                </h3>
                            </div>
                            <div class="intro-slide__image" data-swiper-parallax="-10"><img src="/assets/images/slider-car-1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="intro-slide">
                    <div class="container">
                        <div class="intro-slide__row">
                            <div class="intro-slide__col">
                                <div class="intro-slide__desc">
                                    <p>Toyota Land Cruiser, 4.4D, 2020</p>
                                    <p><strong>{!! __('for') !!} 17,450 USD</strong></p>
                                </div>
                                <h3 class="intro-slide__title">
                                    <big>{!! __('Pay for') !!} 30% </big><span>{!! __('Get your car!') !!}</span>
                                </h3>
                            </div>
                            <div class="intro-slide__image" data-swiper-parallax="-10"><img src="/assets/images/slider-car-2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="intro-slide">
                    <div class="container">
                        <div class="intro-slide__row">
                            <div class="intro-slide__col">
                                <div class="intro-slide__desc">
                                    <p>Mercedes-Benz, 3.0, 2020</p>
                                    <p><strong>{!! __('for') !!} 21,650 USD</strong></p>
                                </div>
                                <h3 class="intro-slide__title">
                                    <big>{!! __('Pay for') !!} 30% </big><span>{!! __('Get your car!') !!}</span>
                                </h3>
                            </div>
                            <div class="intro-slide__image" data-swiper-parallax="-10"><img src="/assets/images/slider-car-3.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-dots"></div>
        <div class="swiper-prev">
            <svg class="svg-icon">
                <use href="/assets/icons/sprite.svg#icon-next"></use>
            </svg>
        </div>
        <div class="swiper-next">
            <svg class="svg-icon">
                <use href="/assets/icons/sprite.svg#icon-next"></use>
            </svg>
        </div>
        <div class="intro__time container">
            <div class="time">
                <div class="time__top">
                    <div class="apply"><img src="/assets/images/apply.png" alt="">
                    </div>
                </div>
                <div class="time__clock">
                    <!--img(src="/assets/images/clock.png", alt="")-->
                    <svg viewBox="0 0 200 200">
                        <filter id="innerShadow" x="-20%" y="-20%" width="140%" height="140%">
                            <fegaussianblur in="SourceGraphic" stddeviation="3" result="blur"></fegaussianblur>
                            <feoffset in="blur" dx="2.5" dy="2.5"></feoffset>
                        </filter>
                        <g>
                            <circle id="shadow" style="fill:transparent" cx="97" cy="100" r="87" filter="url(#innerShadow)"></circle>
                            <circle id="circle" style="stroke: #091361; stroke-width: 4px; fill:#fff" cx="100" cy="100" r="80"></circle>
                        </g>
                        <g>
                            <line id="hourhand" x1="100" y1="100" x2="100" y2="70" transform="rotate(80 100 100)" style="stroke-width: 5px; stroke: #091361;">
                                <animatetransform attributename="transform" attributetype="XML" type="rotate" dur="43200s" repeatcount="indefinite"></animatetransform>
                            </line>
                            <line id="minutehand" x1="100" y1="100" x2="100" y2="60" style="stroke-width: 3px; stroke: #091361;">
                                <animatetransform attributename="transform" attributetype="XML" type="rotate" dur="3600s" repeatcount="indefinite"></animatetransform>
                            </line>
                            <line id="secondhand" x1="100" y1="100" x2="100" y2="45" style="stroke-width: 2px; stroke: #091361;">
                                <animatetransform attributename="transform" attributetype="XML" type="rotate" dur="60s" repeatcount="indefinite"></animatetransform>
                            </line>
                        </g>
                        <circle id="center" style="fill:#fff; stroke: #091361; stroke-width: 2px;" cx="100" cy="100" r="3"></circle>
                    </svg>
                </div>
                <p class="time__title">{!! __('12 months') !!}
                </p>
                <div class="time__desc">
                    <p>{!! __('You`ll receive your car in 12-3 months') !!}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="lines-bg">
        <section class="cards" data-emergence="hidden">
            <div class="container">
                <div class="cards__row">
                    <div class="cards__content">
{{--                        <h4 class="cards__subtitle">{!! __('Calculate') !!}--}}
{{--                        </h4>--}}
                        <h3 class="section-title">{!! __('Car or moto for 30%') !!}
                        </h3>
{{--                        <div class="cards__bottom"><a class="btn btn--line btn--size-md" href="#">{!! __('Calculate') !!}</a>--}}
{{--                        </div>--}}
                    </div>
                    <div class="cards__block">
                        <div class="typography">
                            <p>{{__('WEC AUTO is an investment project of the Web Token Profit ecosystem, created on the basis of the WEC blockchain for the purchase of auto and motorcycle transport by its participants. As part of the auto program, partners can purchase the car of their dreams for only 30-50% of its real value and in a short time.')}}</p>
                        </div>
                        <div class="cards__list">
                            <div class="calc-card">
                                <div class="calc-card__content">
                                    <div class="calc-card__icon">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-profits"></use>
                                        </svg>
                                    </div>
                                    <p class="calc-card__title">{{__('Invest 30%')}}
                                    </p>
                                    <div class="calc-card__desc">
{{--                                        <p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit, sed do eiusmod tempor</p>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="calc-card">
                                <div class="calc-card__content">
                                    <div class="calc-card__icon">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-calendar"></use>
                                        </svg>
                                    </div>
                                    <p class="calc-card__title">{{__('12-3 months')}}
                                    </p>
                                    <div class="calc-card__desc">
{{--                                        <p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit, sed do eiusmod tempor</p>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="calc-card">
                                <div class="calc-card__content">
                                    <div class="calc-card__icon">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-speed"></use>
                                        </svg>
                                    </div>
                                    <p class="calc-card__title">{{__('Up to 3 months with FST')}}
                                    </p>
                                    <div class="calc-card__desc">
{{--                                        <p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit, sed do eiusmod tempor</p>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="calc-card">
                                <div class="calc-card__content">
                                    <div class="calc-card__icon">
                                        <svg class="svg-icon">
                                            <use href="/assets/icons/sprite.svg#icon-shuttle"></use>
                                        </svg>
                                    </div>
                                    <p class="calc-card__title">{{__('FST from')}}<br> $50/m
                                    </p>
                                    <div class="calc-card__desc">
{{--                                        <p>Lorem ipsum dolor sit amet,  consectetur adipiscing elit, sed do eiusmod tempor</p>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="join" data-emergence="hidden">
            <div class="container">
                <div class="join__row">
                    <div class="join__image join__image--has-arrow"><img src="/assets/images/join.png" alt="">
                    </div>
                    <div class="join__content">
                        <h3 class="section-title">{!! __('<span class="color-warning">Join</span> today<br> and get your car') !!}
                        </h3>
                        <div class="typography">
                            <p><strong>{{__('Auto program WEC AUTO')}}</strong> {{__('Allow yourself more!')}}.</p>
                            <p>{{__('Investing in the auto program is made in the WEC cryptocurrency. The purchase / sale of coins is carried out on the internal exchange of the auto program by means of a p2p exchange. Becoming a member of the WEC AUTO investment project - you automatically become the owner of the desired vehicle in the shortest possible time and at a cost convenient for you!')}} </p>
                        </div>
                        <div class="join__content-bottom"><a class="btn btn--line btn--size-md" href="{{route('register')}}">{{__('Join now')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="quotes" data-emergence="hidden">
            <div class="container">
                <div class="quotes__slider swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="quote">
                                <p class="quote__title">WEC/USD
                                </p>
                                <div class="quote__bottom">
                                    <p class="">{{rate('WEC', 'USD')}}
                                    </p>
                                    {{--                            <p class="exc-item-small__change exc-item-small__change--plus"><span>+12.1%</span>--}}
                                    {{--                                <svg class="svg-icon">--}}
                                    {{--                                    <use href="/assets/icons/sprite.svg#icon-arrow"></use>--}}
                                    {{--                                </svg>--}}
                                    {{--                            </p>--}}
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="quote">
                                <p class="quote__title">ACC/USD
                                </p>
                                <div class="quote__bottom">
                                    <p class="">{{rate('ACC', 'USD')}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="quote">
                                <p class="quote__title">FST/USD
                                </p>
                                <div class="quote__bottom">
                                    <p class="">{{rate('FST', 'USD')}}
                                    </p>
                                </div>
                            </div>
                        </div>
{{--                        <div class="swiper-slide">--}}
{{--                            <div class="quote">--}}
{{--                                <p class="quote__title">BIP/USD--}}
{{--                                </p>--}}
{{--                                <div class="quote__bottom">--}}
{{--                                    <p class="">{{rate('BIP', 'USD')}}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="swiper-slide">--}}
{{--                            <div class="quote">--}}
{{--                                <p class="quote__title">BTC/USD--}}
{{--                                </p>--}}
{{--                                <div class="quote__bottom">--}}
{{--                                    <p class="">{{rate('BTC', 'USD')}}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="swiper-slide">--}}
{{--                            <div class="quote">--}}
{{--                                <p class="quote__title">ETH/USD--}}
{{--                                </p>--}}
{{--                                <div class="quote__bottom">--}}
{{--                                    <p class="">{{rate('ETH', 'USD')}}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        {{--                <div class="swiper-slide">--}}
                        {{--                    <div class="exc-item-small">--}}
                        {{--                        <p class="exc-item-small__title">ETH/USD--}}
                        {{--                        </p>--}}
                        {{--                        <div class="exc-item-small__bottom">--}}
                        {{--                            <p class="">4,554.42--}}
                        {{--                            </p>--}}
                        {{--                            <p class="exc-item-small__change exc-item-small__change--plus"><span>-12.1%</span>--}}
                        {{--                                <svg class="svg-icon">--}}
                        {{--                                    <use href="/assets/icons/sprite.svg#icon-arrow"></use>--}}
                        {{--                                </svg>--}}
                        {{--                            </p>--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}
                        {{--                </div>--}}
                    </div>
                </div>
            </div>
        </section>
        <section class="calculate" data-emergence="hidden">
            <div class="container">
                <div class="calculate__row">
                    <div class="calculate__content">
{{--                        <h3 class="section-title"><span class="color-warning">{!! __('Calculate')!!}</span>--}}
{{--                        </h3>--}}
                        <div class="typography">
                            <p>{!! __('Participation in the WECAUTO project is allowed when the client buys a license and makes an initial payment of 30% of the cost of the selected car or motorcycle. Auto deposit is valid for 12 months from the date of the first installment. After the expiration of this period, the client receives the entire amount for the purchase of the car.') !!}</p>
                        </div>
                        <form class="calc-form">
{{--                            <div class="calc-form__col">--}}
{{--                                <div class="field">--}}
{{--                                    <label class="select">--}}
{{--                                        <select>--}}
{{--                                            <option>{{__('Brand')}}</option>--}}
{{--                                        </select>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="calc-form__col">--}}
{{--                                <div class="field">--}}
{{--                                    <label class="select">--}}
{{--                                        <select>--}}
{{--                                            <option>{{__('Model')}}</option>--}}
{{--                                        </select>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="calc-form__col">--}}
{{--                                <div class="separator-field">--}}
{{--                                    <p>or</p>--}}
{{--                                </div>--}}
{{--                                <div class="field field--subtext">--}}
{{--                                    <input type="number" placeholder="{{__('Enter price')}}">--}}
{{--                                    <p class="field__subtext">WEC--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </form>
                        <div class="calc-result">
                            <div class="calc-result__col">
                                <p class="calc-result__count">
                                    <big>0 </big>WEC
                                </p>
                                <div class="calc-result__desc">{{__('12 months')}}
                                </div>
                            </div>
                            <div class="calc-result__col">
                                <p class="calc-result__count color-warning">
                                    <big>0 </big>WEC
                                </p>
                                <div class="calc-result__desc">{{__('3 months')}} <i class="color-warning">FST</i>
                                </div>
                            </div>
                            <div class="calc-result__col">
                                <div class="fst">
                                    <p>{{__('Fast your speed with')}}</p><img src="/assets/images/fst.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="calculate__image"><img src="/assets/images/cars.png" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="become" data-emergence="hidden">
            <div class="become__bike"><img src="/assets/images/bike.png" alt="">
            </div>
            <div class="container">
                <div class="become__row">
                    <ul class="become__counts">
                        <li>
                            <div class="become__count"><img src="/assets/images/counts/5.png" alt="5">
                            </div>
                            <p>{{__('Levels partner program')}}</p>
                        </li>
                        <li>
                            <div class="become__count"><img src="/assets/images/counts/25.png" alt="25">
                            </div>
                            <p>{{__('Up to 48% referral bonus')}}</p>
                        </li>
                        <li>
                            <div class="become__count"><img src="/assets/images/counts/10.png" alt="10">
                            </div>
                            <p>{{__('Levels referral program')}}</p>
                        </li>
                    </ul>
                    <div class="become__content">
                        <h3 class="section-title"> {!!__('<span class="color-warning">Become </span>our partner')!!}
                        </h3>
                        <div class="typography">
                            <p><strong>{!! __('Affiliate Rewards are credited to the FST. The affiliate program provides 5 ranks:') !!}</strong></p>
                            <ul>
                                <li> <a href="#">{{__('Partner')}}</a></li>
                                <li> <a href="#">{{__('Manager')}}</a></li>
                                <li> <a href="#">{{__('Director')}}</a></li>
                                <li> <a href="#">{{__('President')}}</a></li>
                                <li> <a href="#">{{__('Shareholder')}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="become__image"><img src="/assets/images/become.png" alt="Become our partner">
                    </div>
                </div>
            </div>
        </section>
        <section class="refferal-levels" data-emergence="hidden">
            <div class="container">
                <ul>
                    <li>
                        <div class="refferal-level">
                            <big>1 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">10<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>2 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">5<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>3 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>4 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>5 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>6 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>7 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>8 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>9 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">5<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>10 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">10<sup>%</sup>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="news-section">
            <div class="container">
{{--                <ul class="news-list" data-emergence="hidden">--}}
{{--                    <li><a class="news-item" href="news-details.html"><span class="news-item__date">22/02/2020</span>--}}
{{--                            <p class="news-item__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod--}}
{{--                            </p>--}}
{{--                            <p class="news-item__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  eiusmod  tempor incididunt  ut labore et dolore magna aliqua.--}}
{{--                            </p><span class="news-item__arrow">--}}
{{--                  <svg class="svg-icon">--}}
{{--                    <use href="/assets/icons/sprite.svg#icon-right-arrow"></use>--}}
{{--                  </svg></span></a>--}}
{{--                    </li>--}}
{{--                    <li><a class="news-item" href="news-details.html"><span class="news-item__date">22/02/2020</span>--}}
{{--                            <p class="news-item__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod--}}
{{--                            </p>--}}
{{--                            <p class="news-item__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  eiusmod  tempor incididunt  ut labore et dolore magna aliqua.--}}
{{--                            </p><span class="news-item__arrow">--}}
{{--                  <svg class="svg-icon">--}}
{{--                    <use href="/assets/icons/sprite.svg#icon-right-arrow"></use>--}}
{{--                  </svg></span></a>--}}
{{--                    </li>--}}
{{--                    <li><a class="news-item" href="news-details.html"><span class="news-item__date">22/02/2020</span>--}}
{{--                            <p class="news-item__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod--}}
{{--                            </p>--}}
{{--                            <p class="news-item__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  eiusmod  tempor incididunt  ut labore et dolore magna aliqua.--}}
{{--                            </p><span class="news-item__arrow">--}}
{{--                  <svg class="svg-icon">--}}
{{--                    <use href="/assets/icons/sprite.svg#icon-right-arrow"></use>--}}
{{--                  </svg></span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
            </div>
        </section>
        <section class="about" data-emergence="hidden">
            <div class="container">
                <div class="about__row">
                    <div class="about__col">
                        <h3 class="section-title"><span class="color-warning">{{__('More')}} </span>{{__('about Wecauto')}}
                        </h3>
                        <ul class="faq-list"><li>
                                <ul class="accordion">
                                    @foreach(getFaqsList(6) as $faq)
                                        <li class="accordion__item"><a class="accordion__trigger" data-accordion="trigger"><span class="accordion__trigger-overlay"><span>{{ $faq['title'] }}</span></span></a>
                                            <div data-accordion="content-container">
                                                <div class="accordion__content typography" data-accordion="content">
                                                    <p>{{ $faq['text'] }} </p>

                                                </div>
                                            </div>
                                        </li>
                                    @endforeach

                                </ul>
                            <li class="faq-list__bottom"><a class="link-more" href="{{route('customer.faq')}}"> <span>{!! __('Read FAQ') !!}</span>
                                    <svg class="svg-icon">
                                        <use href="/assets/icons/sprite.svg#icon-right-arrow"></use>
                                    </svg></a>
                            </li>
                        </ul>
                    </div>
                    <div class="about__col">
                        <div class="about-counts about-counts--has-arrow">
                            <div class="about-counts__section">
                                <p class="about-counts__title">{{__('Users')}}
                                </p>
                                <p class="about-counts__count">{{getActiveAccounts()}}
                                </p>
                            </div>
                            <div class="about-counts__section">
                                <p class="about-counts__title">{{__('Deposits')}}
                                </p>
                                <p class="about-counts__count">{{getDepositsCount()}}
                                </p>
                            </div>
                            <div class="about-counts__section">
                                <p class="about-counts__title">{{__('FST Sold')}}
                                </p>
                                <p class="about-counts__count">{{number_format(getFstSold(), 2)}}
                                </p>
                            </div>
                            <div class="about-counts__section">
                                <p class="about-counts__title">{{__('Happy owners')}}
                                </p>
                                <p class="about-counts__count">{{getClosedDepositsCount()}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
{{--        <section class="reviews-section">--}}
{{--            <div class="container">--}}
{{--                <ul class="reviews-list" data-emergence="hidden">--}}
{{--                    <li>--}}
{{--                        <div class="review-item">--}}
{{--                            <div class="review-item__avatar"><img src="/assets/images/reviews/ava.png" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="review-item__content">--}}
{{--                                <p class="review-item__name">Ivan Petrov--}}
{{--                                </p>--}}
{{--                                <div class="typography">--}}
{{--                                    <p>Over the last few years, leading investment companies began to size up a new, young cryptocurrency market. In 2018, institutional investors started entering this market, we are no exception cryptocurrency market. In 2018, institutional investors started entering this market, we are no exception. </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <div class="review-item">--}}
{{--                            <div class="review-item__avatar"><img src="/assets/images/reviews/ava.png" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="review-item__content">--}}
{{--                                <p class="review-item__name">Ivan Petrov--}}
{{--                                </p>--}}
{{--                                <div class="typography">--}}
{{--                                    <p>Over the last few years, leading investment companies began to size up a new, young cryptocurrency market. In 2018, institutional investors started entering this market, we are no exception cryptocurrency market. In 2018, institutional investors started entering this market, we are no exception. </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <div class="review-item">--}}
{{--                            <div class="review-item__avatar"><img src="/assets/images/reviews/ava.png" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="review-item__content">--}}
{{--                                <p class="review-item__name">Ivan Petrov--}}
{{--                                </p>--}}
{{--                                <div class="typography">--}}
{{--                                    <p>Over the last few years, leading investment companies began to size up a new, young cryptocurrency market. In 2018, institutional investors started entering this market, we are no exception cryptocurrency market. In 2018, institutional investors started entering this market, we are no exception. </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="video-list" data-emergence="hidden">--}}
{{--                    <li><a class="video-item" href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" data-fancybox="video-list"><img src="https://dummyimage.com/356x219/fff/aaa" alt=""></a>--}}
{{--                    </li>--}}
{{--                    <li><a class="video-item" href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" data-fancybox="video-list"><img src="https://dummyimage.com/356x219/fff/aaa" alt=""></a>--}}
{{--                    </li>--}}
{{--                    <li><a class="video-item" href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" data-fancybox="video-list"><img src="https://dummyimage.com/356x219/fff/aaa" alt=""></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <div class="reviews-section__bottom"><a class="btn btn--line btn--size-md" href="#">Show more</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
    </div>
@endsection
@push('load-scripts')
@endpush