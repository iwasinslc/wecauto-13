@extends('layouts.auth')
@section('title', __('Privacy Policy'))
@section('content')
    <section class="page-preview">
        <div class="container">
                                <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('Privacy Policy')}}</span>
            </div>
            <h1 class="page-title">{{__('Wecauto Privacy Policy')}}
                    </h1>
                </div>
                 </section>
    <section class="terms">
        <div class="container">
            <div class="typography">
                <p class="typography-intro-text"> <strong> 
                    <h4>{!! __('1.	General') !!}</h4>
                    <p>{!! __('1.1. This Privacy Policy defines and explains the procedure for transferring, processing and storing personal data of users and customers, is based on legal requirements and is intended to ensure the security of personal information of each participant, as well as prevent unauthorized dissemination of confidential data.') !!}</p>
                    <p>{!! __('1.2. The Wecauto platform administration respects to unconditionally observe human rights and freedoms in the course of cooperation, processing and personal data usage.') !!}</p>
                    <p>{!! __('1.3. This Privacy Policy applies to all confidential data, which administration can get from the Crypto Accelerator platform visitor.') !!}</p>
                    <p>{!! __('1.4. The platform participant agrees and undertakes all measures to protect his or her account from outside interference and access to his or her data and financial resources by third parties. Webtokenprofit is not responsible for information leakage caused by the user activities or disregard for security measures. Moreover, if suspicious activity is detected, the user may be warned or forced to block his/her account due to cooperation rule violation.') !!}</p>
                    <h4>{!! __('2. The following information is subject to processing:') !!}</h4>
                    <p>{!! __('2.1. Full name (surname, first name, patronymic);') !!} </p>
                    <p>{!! __('2.2. Contact information including telephone number and e-mail address;') !!}</p>
                    <p>{!! __('2.3. Anonymized information including cookies, used browser data, operating system, geolocation, and other information collected by online analytic and metric services;') !!}</p>
                    <p>{!! __('2.4. Payment details with which the user makes a deposit and withdraws funds from the system.') !!}</p>
                    <p>{!! __('2.5. All of the above-mentioned data are united by a single concept of "User Personal Data".') !!}</p>
                    <p>{!! __("2.6.	The Company guarantees confidentiality and protects any Customer's data from third parties under the terms of the Privacy Policy.") !!}</p>
                    <h4>{!! __('3. Purposes of Confidential Data Processing') !!}</h4>
                    <p>{!! __("3.1. The Administration processes user's personal data to interact with individuals via the Internet, to provide mutually beneficial cooperation, the possibility of providing users with access to platform services and materials, receiving funds and payments within financial relations.") !!}</p>
                    <p>{!! __("3.2. The Company has the right to use the user's e-mail address to notify about changes in the platform's operation, new product and service introduction, bonuses and promotions, to send other information related to the service operation. The user may refuse from such mailings.") !!}</p>
                    <p>{!! __('3.3. Anonymized information is collected in order to record and analyze the visitor performance on the platform. This allows finding possible vulnerabilities and defects in the website operation, eliminating them and improving service quality, as well as developing new tools for more useful interaction.') !!}</p>
                     <h4>{!! __('4. Personal data transfer methods') !!}</h4>
                    <p>{!! __("4.1. The user's personal information is transferred to the platform operators when registering and filling in the questionnaire using appropriate forms placed on the website. By filling in the personal data fields, the user automatically agrees to their processing.") !!}</p>
                    <p>{!! __("4.2. Anonymized data are processed only if the user's browser has the appropriate settings for their transfer and JavaScript technology is activated.") !!}</p>
                   <h4>{!! __('5. Securing confidential data') !!}</h4>
                    <p>{!! __('5.1. Security of confidential information provided by users is ensured by taking necessary organizational and engineering measures in accordance with the law.') !!}</p>
                    <p>{!! __("5.2. The Wecauto website utilizes advanced encryption technologies and enforced path. The resource has received an SSL-certificate, which does not allow fraudsters to intercept or change users' personal data.") !!}</p>
                    <p>{!! __('5.3. Operators who are responsible for data receiving and processing use all reasonable efforts to protect confidential data from unauthorized access by unauthorized persons. An exception is if the data transfer to third parties is made upon request in order to comply with statues in place.') !!}</p>
                    <p>{!! __('5.4. If you indicate inaccuracies in personal data or willfully misleading platform operators, you will receive a notification about the need to update the data, or, in some cases, this will lead to the blocking of your account.') !!}</p>
                    <p>{!! __('5.5. Personal data processing is not limited in time, but it is not performed, of cooperation with the user was terminated. The customer has the right to delete all his/her data provided during registration or filling in the form.') !!}</p>
                    <h4>{!! __('6. Cross-border data transfer') !!}</h4>
                    <p>{!! __('6.1. Before cross-border transferring of personal data, the administration checks whether the resident state provides reliable protection of the rights of citizens who transmit confidential information.') !!}</p>
                    <p>{!! __('6.2. Any disputable situations related to the Privacy Policy are resolved in accordance with legal norms of the country in which the user lives.') !!}</p>
                    <h4>{!! __('7. Final clauses') !!}</h4>
                    <p>{!! __('7.1. Any changes can be made herein, which users are may not be additionally notified about. In order to stay informed about the current Privacy Policy version, we recommend you to visit this page once in a while and review its contents.') !!}</p>
                    <p>{!! __('7.2. The Policy is valid indefinitely, until/if it is replaced by a new version.') !!}</p>
                    <p>{!! __('7.3. If the user has any questions concerning the subject of the Privacy Policy and the personal data processing, he/she can get any explanations in that respect when contacting the operator of the support service via the feedback form or by e-mail.') !!}</p>

                </div>
            </div>
        </section>
    </article>
@endsection