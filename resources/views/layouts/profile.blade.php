<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/css/common.css?v={{rand(1,10000000)}}">
    <link rel="stylesheet" href="/assets/css/lk.css?v={{rand(1,10000000)}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .dataTables_filter {
            display: none;
        }
    </style>
    <script>window.PUSHER_APP_KEY = '{{ config('broadcasting.connections.pusher.key') }}';</script>
    <script>window.PORT = 2053;</script>
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script>
        window.__lc = window.__lc || {};
        window.__lc.license = 12063651;
        ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
    </script>
    <noscript><a href="https://www.livechatinc.com/chat-with/12063651/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
    <!-- End of LiveChat code -->
    <!-- End of LiveChat code -->
</head>


<body>
<div class="loader"><img src="/assets/images/spinner.svg" alt=""></div>
<main class="wrapper" id="app" data-emergence="hidden">
<div class="lk content">
{{--    @if (!user()->isGoogle2FaEnabled())--}}
{{--        <div class="lk__message">--}}
{{--            <div class="container">--}}
{{--                <p>{{__('Your account isn’t secure')}}</p><a class="btn btn--platinum-dark"--}}
{{--                                                             href="{{route('profile.settings')}}">{{__('Turn ON Google Autentificator')}}</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}

    <div class="lk__header">
        <div class="header-lk">
            <div class="container"><a class="header-lk__logo" href="{{route('profile.profile')}}"><img src="/assets/images/logo-white.png" alt=""></a>
                <nav class="header-lk__nav">
                    <ul>
                        <li><a href="{{route('profile.profile')}}">{{__('Account')}}</a></li>
{{--                        <li><a target="_blank" href="{{route('customer.support')}}">{{__('E-mail us')}}</a></li>--}}
                        <li><a style="color: red" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form1').submit();">{{__('Logout')}}</a>
                            <form id="logout-form1" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">{{ csrf_field() }}</form>
                        </li>
                    </ul>
                </nav>
                <div class="header-lk__separator">
                </div>
                <div class="header-lk__right">
                    <div class="language-module js-dropdown">
                        <div class="language-module__link">
                            <div class="language-module__image"><img src="/assets/images/country/{{app()->getLocale()}}.png"
                                                                     alt="">
                            </div>
                            <span>{{strtoupper(app()->getLocale())}}</span>
                        </div>
                        <ul class="language-module__dropdown">
                            @foreach(getLanguagesArray() as $key => $item)
                                <li>
                                    <a href="{{ route('set.lang',['locale'=>$item['code']]) }}">
                                                        <span class="language-module__image">
                                                                                                                                    <img src="/assets/images/country/{{$item['code']}}.png"
                                                                                                                                         alt="">
                                                                                                                                </span>
                                        <span>{{$item['name']}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <ul class="day-info">
                        <li>
                            <p>{{now()->format('d')}} {{now()->format('M')}} <span>/{{now()->format('y')}}</span></p>
                        </li>
                        <li>
                            <p>{{now()->format('H:i:s')}}</p>
                        </li>

                    </ul>

                    @php
                    $wallets = [
                        'usd' => getUserWallet('USD'),
                        'acc' => getUserWallet('ACC'),
                        'wec' => getUserWallet('WEC'),
                        'pzm' => getUserWallet('PZM'),
                        'fst' => getUserWallet('FST')
                    ];
                    @endphp

                    <div class="balance-module">
                        <div class="balance-module__count"><span>$</span><b id="balance_{{$wallets['usd']->id}}">{{number_format($wallets['usd']->balance, 2)}}</b>
                        </div>
                        <div class="balance-module__count"><b id="balance_{{$wallets['acc']->id}}">{{number_format($wallets['acc']->balance, 4)}}</b> <span>{{$wallets['acc']->currency->code}}</span>
                        </div>
                        <div class="balance-module__count"><b id="balance_{{$wallets['wec']->id}}">{{number_format($wallets['wec']->balance, 4)}}</b> <span>{{$wallets['wec']->currency->code}}</span>
                        </div>
                        <div class="balance-module__count"><b id="balance_{{$wallets['pzm']->id}}">{{number_format($wallets['pzm']->balance, 4)}}</b> <span>{{$wallets['pzm']->currency->code}}</span>
                        </div>
                        <div class="balance-module__count"><b id="balance_{{$wallets['fst']->id}}">{{number_format($wallets['fst']->balance, 4)}}</b> <i>{{$wallets['fst']->currency->code}}</i>
                        </div>
                    </div>
                </div>
                <ul class="account-buttons">
                    <li><a href="{{route('customer.main')}}">
                            <svg class="svg-icon">
                                <use href="/assets/icons/sprite.svg#icon-home"></use>
                            </svg></a>
                    </li>
                    <li class="account-buttons__notifications"><a class="js-open-notifications"><span class="account-buttons__count"></span>
                            <svg class="svg-icon">
                                <use href="/assets/icons/sprite.svg#icon-bell"></use>
                            </svg></a>
                    </li>
                    <li class="account-buttons__username"><a href="{{route('profile.settings')}}"><span
                                    class="account-buttons__avatar">
                  <!-- AVATAR DUMMY-->
                                <!-- AVATAR IMAGE--><img class="js-save-crop-image" src="{{user()->getAvatarPath()}}"
                                                         alt=""></span><span>{{user()->login}}</span></a>
                </ul>
            </div>
        </div>
    </div>
    <div class="lk__row">
        @include('partials.profile.menu')
        <article class="lk__content">
            <!-- Begin page-->
            @include('partials.profile.exchange_list')
{{--            @if(!user()->isVerifiedEmail())--}}
{{--                @include('partials.message')--}}
{{--            @endif--}}
            @include('partials.profile.balances')
            @yield('content')
        </article>
    </div>

    <div class="lk__footer">
        <footer class="footer">
            <div class="container">
                <div class="footer__row">
                    <div class="footer__col"><a class="footer__logo" href="{{route('customer.main')}}"><img
                                    src="/assets/images/logo-gray.png?v=2" alt=""></a>
                        <nav class="footer__nav">
                            <ul>
{{--                                <li class="{{ (Route::is('customer.about-us') ? 'active' : '') }}"><a--}}
{{--                                            href="{{route('customer.about-us')}}">{{__('Marketing')}}</a>--}}
{{--                                </li>--}}
                                <li class="{{ (Route::is('customer.faq') ? 'active' : '') }}"><a
                                            href="{{route('customer.faq')}}">{{__('FAQ')}}</a>
                                </li>
{{--                                <li class="{{ (Route::is('customer.support') ? 'active' : '') }}"><a--}}
{{--                                            href="{{route('customer.support')}}">{{__('Contacts')}}</a>--}}
{{--                                </li>--}}
                            </ul>
                        </nav>
                    </div>
{{--                    <div class="footer__col">--}}
{{--                        <div class="follow">--}}
{{--                            <p class="follow__title">{{__('Follow us at social network')}}:--}}
{{--                            </p>--}}
{{--                            <ul>--}}
{{--                                <li><a href="#" target="_blank">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-vk"></use>--}}
{{--                                        </svg>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li><a href="#" target="_blank">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-twitter"></use>--}}
{{--                                        </svg>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li><a href="#" target="_blank">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-instagram"></use>--}}
{{--                                        </svg>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li><a href="#" target="_blank">--}}
{{--                                        <svg class="svg-icon">--}}
{{--                                            <use href="/assets/icons/sprite.svg#icon-facebook"></use>--}}
{{--                                        </svg>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <div class="footer__bottom">
                    <p>© 2020, WEC Auto. {{__('All rights reserved')}}.</p>
                    <ul>
                        <li><a href="{{route('customer.agreement')}}">{{__('Terms and Conditions')}}</a></li>
                        <li><a href="{{route('customer.privacy')}}">{{__('Privacy Policy')}}</a></li>

                    </ul>
                </div>
            </div>
        </footer>
    </div>
    <div class="notifications">


        @if(session()->has('success'))

            <div class="notification-item notification-item--gray"><a
                        class="notification-item__close js-remove-notification">
                    <svg class="svg-icon">
                        <use href="/assets/icons/sprite.svg#icon-cross"></use>
                    </svg>
                </a>
                <p>{{session()->get('success')}}</p>
            </div>
        @endif

        @if(session()->has('status'))
            <div class="notification-item notification-item--gray"><a
                        class="notification-item__close js-remove-notification">
                    <svg class="svg-icon">
                        <use href="/assets/icons/sprite.svg#icon-cross"></use>
                    </svg>
                </a>
                <p>{{session()->get('status')}}</p>
            </div>
        @endif

        @if(session()->has('error'))
            <div class="notification-item notification-item--warning"><a
                        class="notification-item__close js-remove-notification">
                    <svg class="svg-icon">
                        <use href="/assets/icons/sprite.svg#icon-cross"></use>
                    </svg>
                </a>
                <p>{{session()->get('error')}}</p>
            </div>
        @endif

        @if ($errors->any())


            @foreach ($errors->all() as $error)
                <div class="notification-item notification-item--warning"><a
                            class="notification-item__close js-remove-notification">
                        <svg class="svg-icon">
                            <use href="/assets/icons/sprite.svg#icon-cross"></use>
                        </svg>
                    </a>
                    <p>{{$error}}</p>
                </div>
            @endforeach

        @endif

            @foreach (user()->notifications as $notification)

                @if ($notification->type === 'success')
                    <div class="notification-item notification-item--gray" ><a
                                class="notification-item__close js-remove-notification notify-remove"  data-id="{{$notification->id}}">
                            <svg class="svg-icon">
                                <use href="/assets/icons/sprite.svg#icon-cross"></use>
                            </svg>
                        </a>
                        <p>{{$notification->created_at->format('d-m-Y H:i:s')}} {{$notification->translate(user())}}</p>
                    </div>
                @else

                    <div class="notification-item notification-item--warning"><a
                                class="notification-item__close notify-remove js-remove-notification" data-id="{{$notification->id}}">
                            <svg class="svg-icon">
                                <use href="/assets/icons/sprite.svg#icon-cross"></use>
                            </svg>
                        </a>
                        <p>{{$notification->created_at->format('d-m-Y H:i:s')}} {{$notification->translate(user())}}</p>
                    </div>

                @endif

            @endforeach

    </div>
    @yield('modal_content')

</div>

<!-- Modals-->

<!-- Success modal-->
<div class="modal" id="success">
  <div class="modal-inner modal-inner--dark">
    <div class="modal-inner__close js-close-modal">
      <svg class="svg-icon">
        <use href="/assets/icons/sprite.svg#icon-cross"></use>
      </svg>
    </div>
    <div class="success-modal">
      <h3 class="success-modal__title">Спасибо!
      </h3>
      <div class="success-modal__icon">
        <svg class="svg-icon">
          <use href="/assets/icons/sprite.svg#icon-check"></use>
        </svg>
      </div>
      <div class="success-modal__text">
        <p>Ваша заявка принята. Ожидайте ответа в течении 5 суток.</p>
      </div>
      <div class="modal-footer modal-footer--center">
        <button class="btn btn--warning btn--size-lg js-close-modal"><span>Закрыть окно</span>
        </button>
      </div>
    </div>
  </div>
</div>
<!-- Error modal -->
<div class="modal" id="error">
  <div class="modal-inner modal-inner--warning">
    <div class="modal-inner__close js-close-modal">
      <svg class="svg-icon">
        <use href="/assets/icons/sprite.svg#icon-cross"></use>
      </svg>
    </div>
    <div class="success-modal">
      <h3 class="success-modal__title">Ошибка!
      </h3>
      <div class="success-modal__icon">
        <svg class="svg-icon">
          <use href="/assets/icons/sprite.svg#icon-warning"></use>
        </svg>
      </div>
      <div class="success-modal__text">
        <p>Вы не прикрепили файл договора. Вернитесь и загрузите его скан-копию.</p>
      </div>
      <div class="modal-footer modal-footer--center">
        <button class="btn btn--primary btn--size-lg js-close-modal"><span>Вернуться к заявке</span>
        </button>
      </div>
    </div>
  </div>
</div>

</main>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>



<script src="/admin_assets/js/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/js/jquery.scrollbar.js?v={{rand(1,10000000)}}"></script>
<script src="/assets/js/vendors.js?v={{rand(1,10000000)}}"></script>
<script src="/assets/js/lk.js?v={{rand(1,10000000)}}"></script>



<script>
    $(document).on("wheel", "input[type=number]", function (e) {
        $(this).blur();
    });
    $('#topup_cur').change(function () {
        val = $(this).val();
        $('#topup_button').attr('data-modal', 'topup_'+val);
    });


    $('#withdraw_cur, #withdraw_cur_modal').change(function () {
        val = $(this).val();
        code = $(this).find('option:selected').html();

        if (code == 'WEC') {
            $('.wcom').hide();
        } else {
            $('.wcom').show();
        }
        $.ajax({
            type: 'get',
            url: '{{route('profile.systems', ['type'=>'withdraw'])}}',
            datatype: 'json',
            data: {currency: val},
            success: function (data) {
                var list = '';
                $.each(data, function (index, value) {
                    $.each(value['currencies'], function (i, v) {
                        list += '<option  value="' + v['id'] + ':' + value['id'] + '">' + value['name'] + ' ' + v['code'] + '</option>';

                    });

                });



                $('#withdraw_ps_list').html(list);
                $('#withdraw_cur').find('option[value="' + val + '"]').prop('selected', true);
                $('#withdraw_cur_modal').find('option[value="' + val + '"]').prop('selected', true);

            },
            error: function () {
            }
        });
    });


    $('body').on('change', '#withdraw_ps_list', function () {
        code = $(this).find('option:selected').html();

        if (code == 'Prizm PZM')
        {
            $('#withdraw .modal-content').append('<div class="balance-form__field">\n' +
                '                        <label>{{__('Comment')}}:</label>\n' +
                '\n' +
                '                        <input  type="text" value="" name="comment" >\n' +
                '\n' +
                '</div>');
        }

    });







    $('body').on('change, keyup', '#topup_amount', function () {
        var val = $(this).val();
        var code = $('#topup_cur').val();
        $( '#topup_'+code+' #topup_amount_modal').val(val);
    });

    $('body').on('change, keyup', '#topup_amount_modal', function () {
        var val = $(this).val();
        $('#topup_amount').val(val);
    });

    $('body').on('change, keyup', '#withdraw_amount', function () {
        var val = $(this).val();
        $('#withdraw_amount_modal').val(val);
    });

    $('body').on('change, keyup', '#withdraw_amount_modal', function () {
        var val = $(this).val();
        $('#withdraw_amount').val(val);
    });

    $(document).ready(function () {
        const timeout = 900000 * 4;  // 900000 ms = 15 minutes
        var idleTimer = null;
        $('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
            clearTimeout(idleTimer);

            idleTimer = setTimeout(function () {
                document.getElementById('logout-form1').submit();
            }, timeout);
        });
        $("body").trigger("mousemove");
    });

</script>
<script>


    {{--$(document).ready(function () {--}}
    {{--$.ajax({--}}
    {{--    type: 'get',--}}
    {{--    url: '{{route('profile.check_amount')}}',--}}
    {{--    datatype: 'json',--}}
    {{--    // data: {currency: val},--}}
    {{--    success: function (data) {--}}

    {{--        if (data.need_modal==1)--}}
    {{--        {--}}
    {{--            window.Modal.openModal('need_pay');--}}
    {{--        }--}}

    {{--    },--}}
    {{--    error: function () {--}}
    {{--    }--}}
    {{--});--}}
    {{--});--}}

    @if(session()->has('success'))
    window.Notice.openSuccess('{{session()->get('success')}}', 5000);
    @endif

    @if(session()->has('status'))
    window.Notice.openSuccess('{{session()->get('status')}}', 5000);
    @endif

    @if(session()->has('error'))
    window.Notice.openError('{{session()->get('error')}}', 5000);
    @endif

    @if ($errors->any())


    @foreach ($errors->all() as $error)
    window.Notice.openError('{{$error}}', 5000);
    @endforeach

    @endif


</script>
<script>


    window.Echo.private('balance.user.{{user()->id}}').listen(".my-balance-updated", (e) => {
        console.log(e);
        // data = e.data;
        // console.log(data);
         $('#balance_' + e.balanceData.id+', .balance_' + e.balanceData.id).html(e.balanceData.balance)

    });


    window.Echo.private('user.{{user()->id}}').listen("NotificationEvent", (e) => {
        data = e.data;
         console.log($('.notifications').length);
        let notif = '';
        if(data.type=='success')
        {
            window.Notice.openSuccess(data.message);
            notif = '<div class="notification-item notification-item--gray" ><a class="notification-item__close js-remove-notification notify-remove"  data-id="'+data.id+'">';
            notif+='<svg class="svg-icon">';
            notif+='<use href="/assets/icons/sprite.svg#icon-cross"></use>';
            notif+='</svg>';
            notif+='</a>';
            notif+='<p>'+data.created_at+' '+data.message+'</p>';
            notif+='</div>';


        }
        else
        {
            window.Notice.openError(data.message);

            notif = '<div class="notification-item notification-item--warning" ><a class="notification-item__close js-remove-notification notify-remove"  data-id="'+data.id+'">';
            notif+='<svg class="svg-icon">';
            notif+='<use href="/assets/icons/sprite.svg#icon-cross"></use>';
            notif+='</svg>';
            notif+='</a>';
            notif+='<p>'+data.created_at+' '+data.message+'</p>';
            notif+='</div>';
        }

        let audio = new Audio('/audio/maybe-one-day.mp3');
        audio.play();

        $('.notifications').prepend(notif);

        let count = $('.notification-item').length;
        $('.account-buttons__count').html(count);
        $('.account-buttons__count').show();
        
        if ($('.has-notifications').length==0)
        {
            $('.account-buttons__notifications').addClass('has-notifications');
        }




    });




    function clear_form($form) {
        $form.find('input[type="text"]').val('');
        $form.find('input[type="number"]').val(0);
    }


    $('.ajax-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var data = $(this).serialize();

        $.ajax({
            url: $(this).attr('action'),
            data: data,
            // headers: {
            //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            // },
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {

                window.Notice.openSuccess(data.success, 10000);
                clear_form(form);
            },
            error: function (data) {
                var errors = data.responseJSON;
                console.log(errors);


                $.each(errors.errors, function (key, value) {
                    window.Notice.openError(value, 10000);
                });


            }
        });

    });
</script>
@stack('load-scripts')

</body>

</html>