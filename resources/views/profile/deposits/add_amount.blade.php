@extends('layouts.profile')
@section('title', __('Car order'))
@section('content')
    <section class="order-step">
        <div class="container">
            <div class="car-orders">
                <h3 class="lk-title">{{__('Car order')}}
                </h3>
            </div>
            <ul class="steps-indicator">
                <li class="is-active"> <span>1</span>
                </li>
                <li class="is-active"> <span>2</span>
                </li>
                <li class="is-active is-current"> <span>3</span>
                </li>
                <li> <span>4</span>
                </li>
            </ul>
            <div class="steps-title">
                <h4><span class="color-warning">{{__('Step')}}</span> #3</h4><small>{{__('Add amount')}}</small>
            </div>
            <form method="post" enctype="multipart/form-data" action="{{route('profile.deposits.add_amount_handle')}}">
                {{csrf_field()}}
            <div class="vehicle-value">
                <div class="vehicle-value__row">

                    <div class="vehicle-value__content">
                        <div class="steps-intro">
                            <p>{{__('Enter vehicle value in USD')}}<br> {{__('up to the maximum limit of your license')}}</p>
                        </div>
                        <div class="field field--subtext">
                            <input type="number" name="amount" id="input"  step="any" value="{{isset($data['invested']) ? $data['invested'] : 0}}">
                            <input type="hidden" name="wallet_id" value="{{getUserWallet('WEC')->id}}">
                            <input type="hidden" name="type" value="{{$data['type']}}">
                            <p class="field__subtext">USD
                            </p>
                        </div>
                        <div class="vehicle-value__row">
                            <div class="vehicle-value__col">
                                <h3>{{__('Down payment')}} 30%</h3>
                                <div class="field field--subtext">
                                    <input id="usd_input" type="text" readonly value="{{isset($data['invested']) ? $data['invested']*0.3 : 0}}">
                                    <p class="field__subtext">USD
                                    </p>
                                </div>
                            </div>
                            <div class="vehicle-value__col">
                                <div class="field field--subtext">
                                    <input id="wec_input" type="text" readonly value="{{isset($data['invested']) ? $data['invested']*0.3*rate('USD', 'WEC') : 0}}">
                                    <p class="field__subtext">WEC
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vehicle-value__image">
                        <div class="vehicle-value__car-image"><img src="{{getPhotoPath(isset($data['image']) ? $data['image'] : '')}}" alt="">
                        </div>
                    </div>

                </div>
            </div>
            <div class="order-step__buttons">
                <button class="btn btn--warning btn--size-md" type="submit">{{__('Next step')}}</button>
            </div>
            </form>
        </div>
    </section>
    <!-- /.card -->
@endsection

@push('load-scripts')
    <script>
                $('body').on('change, keyup', '#input', function () {
                    var val = $(this).val();
                    var rate = {{rate('USD', 'WEC')}};
                    $('#usd_input').val(val*0.3);
                    $('#wec_input').val(val*rate*0.3);
                });
    </script>
@endpush