@extends('layouts.profile')
@section('title', __('Add FST'))
@section('content')
    <section>
        <div class="container">
            <form action="{{route('profile.deposits.add_fst_handle')}}" method="post">
                {{csrf_field()}}
                <div class="add-fst">
                <div class="add-fst__row">
                    <div class="add-fst__col">
                        <h3 class="lk-title">{{__('FST activation')}}
                        </h3>
                        <div class="typography">
                            <h3>{{__('Speed up your application up to 90 days with FST technology')}}</h3>
                            <p>{!! __('Enter the desired number of days to complete the application for your vehicle and calculate the FST amount to activate the term. Maximum accelerated execution time - 90 days from the moment of activation 20% of the vehicle cost in FST') !!}</p>
                        </div>
                        <div class="fst-form">
                            <div class="fst-form__row">
                                <div class="fst-form__col">
                                    <div class="field field--subicon">
                                        <label>{{__('Days to Receive')}}</label>
                                        <input type="number" id="count" value="{{$deposit->minNumberDays()}}" min="{{$deposit->minNumberDays()}}" name="count" max="{{$deposit->maxNumberDays()}}">
                                        <input type="hidden" name="deposit_id" value="{{$deposit->id}}">
                                        <input type="hidden" name="wallet_id" value="{{getUserWallet('FST')->id}}">
                                        <div class="field__subicon">
                                            <svg class="svg-icon">
                                                <use href="/assets/icons/sprite.svg#icon-calendar2"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="fst-form__col">
                                    <div class="field field--subtext">
                                        <label>{{__('FST amount')}}</label>
                                        <input type="text" id="amount" value="{{$deposit->allowedExecutionDays() * $deposit->dayFst(true)}}">
                                        <div class="field__subtext">FST
                                        </div>
                                        <div class="messages">
                                            <p id="amount_usd">{{$deposit->allowedExecutionDays() * $deposit->dayFst()}} USD</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="add-fst__col">
                        <div class="add-fst__fst-image"><img src="/assets/images/fst2.png" alt="">
                        </div>
                        <div class="add-fst__car-image"><img src="{{getPhotoPath($deposit->image)}}" alt="">
                        </div>
                    </div>
                </div>
                <p class="order-card__title"><strong>{{$deposit->name}} </strong></p>
                <div class="v-indicator">
                    <div class="v-indicator__bar"><span style="width:{{$deposit->progressPercentage()}}%"><span>{{now()->format('d/m')}}<span>‘{{now()->format('y')}}</span></span></span>
                    </div>
                    <p class="v-indicator__start-date">{{$deposit->created_at->format('d/m')}} <span>‘{{$deposit->created_at->format('y')}}</span>
                    <p class="v-indicator__end-date">{{$deposit->datetime_closing->format('d/m')}} <span>‘{{$deposit->datetime_closing->format('y')}}</span>
                    </p>
                </div>
                <!-- (class="--type-bike")-->
                <!-- (class="--warning")-->
                <div class="v-indicator v-indicator--warning">
                    <div class="v-indicator__fst"><img src="/assets/images/fst2.png" alt="">
                    </div>
                    <div class="v-indicator__bar"><span style="width:{{$deposit->progressPercentage($deposit->allowedExecutionDays())}}%"><span>{{now()->format('d/m')}}<span>‘{{now()->format('y')}}</span></span></span>
                    </div>
                    <p class="v-indicator__start-date">{{$deposit->created_at->format('d/m')}} <span>‘{{$deposit->created_at->format('y')}}</span>
                    <p class="v-indicator__end-date">{{$deposit->datetime_closing->subDays($deposit->allowedExecutionDays())->format('d/m')}} <span>‘{{$deposit->datetime_closing->subDays($deposit->allowedExecutionDays())->format('y')}}</span>
                    </p>
                </div>
                <div class="add-fst__bottom">
                    <button class="btn btn--warning"><span>{{__('Activate')}}</span><img src="/assets/images/fst-white2.png" alt="">
                    </button>
                </div>
            </div>
            </form>
        </div>
    </section>
    <!-- /.card -->
@endsection

@push('load-scripts')
    <script>
        $('body').on('change, keyup', '#count', function () {
            var input = $(this),
                value = parseInt(input.val()),
                min = parseInt(input.attr('min')),
                max = parseInt(input.attr('max'));





            if(value >= max) {
                input.val(max - 1);
                value = max - 1;
            } else if(value < 0 || !value) {
                value = 0;
                $('#amount_usd').html('0 USD');
                $('#amount').val(0);
                return;
            }



            var count = max - value; // days on which number
            var rate = {{rate('FST', 'USD')}};
            var dm = {{$deposit->dayFst(true)}};
            var full = count*dm;
            if(full<0)
            {
                full = 0;
            }
            $('#amount_usd').html((full*rate)+' USD');
            $('#amount').val(full);

            $.ajax({
                type: 'get',
                url: '{{route('profile.deposits.change_days', ['id'=>$deposit->id])}}',
                datatype: 'json',
                data: {count: count},
                success: function (data) {
                    $('.v-indicator.v-indicator--warning').find('.v-indicator__bar span').css('width', data.percent+'%');
                    $('.v-indicator.v-indicator--warning').find('.v-indicator__end-date').html(data.end_date_month+' <span>‘'+data.end_date_year+'</span>');
                },
                error: function () {
                }
            });

        });


        {{--$(document).ready(function () {--}}

        {{--});--}}
    </script>
@endpush