@extends('layouts.profile')
@section('title', __('Account'))
@section('content')


    <section>
        <div class="container">
            <h3 class="lk-title">{!! __('Buy license') !!}
            </h3>
            <div class="license-block">
                <div class="license-block__content">
                    <h3 class="license-block__title"><span>{{user()->activeLicence() ? user()->licence->name : __('No license')}}</span>
                        @if (user()->activeLicence())
                        <p class="license-block__price"><span><span class="color-warning">$</span>{{user()->licence->price}}</span>
                        </p>
                            @endif
                    </h3>
{{--                    <div class="typography">--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>--}}
{{--                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>--}}
{{--                    </div>--}}
                </div>
                <div class="license-block__aside">
                    @if (user()->activeLicence())
                    <ul>
                        <li>{!! __('Price') !!} -  <strong> {{user()->licence->price*rate('USD', user()->licence->currency->code)}} {{user()->licence->currency->code}}  </strong>
                        </li>
                        <li>{!! __('Minimum car price') !!} - <strong>${{user()->licence->deposit_min}}</strong></li>
                        <li>{!! __('Minimum motorcycle price') !!} - <strong>${{user()->licence->moto_min}}</strong></li>
                        <li>{!! __('The most affordable price for a car or motorcycle') !!} - <strong>${{user()->licence->deposit_max}}</strong></li>
                        <li>{!! __('Maximum purchase amount FST') !!} <strong>${{user()->licence->buy_amount}}</strong>
                        </li>
                        <li>{!! __('Maximum FST Sale Amount') !!} <strong>${{user()->licence->sell_amount}}</strong>
                        </li>
                    </ul>
                    <p class="license-block__aside-price">{!! __('Car or moto price') !!} ${{user()->licence->deposit_max}} <small>({!! __('max') !!})</small>
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section class="licenses">
        <div class="container">
            <h3 class="lk-title">{!! __('Licenses') !!}
            </h3>
            <div class="js-swiper-licenses swiper-container swiper-no-swiping">
                <div class="swiper-wrapper">
                    @foreach($licences as $licence)
                        <div class="swiper-slide">0
                            <div class="license-item {{!user()->canBuyLicence()&&$licence->id==user()->licence_id? 'license-item--active' : ''}}">
                                <p class="license-item__name">{{__($licence->name)}} {{$licence->id}}
                                </p>
                                <div class="license-item__price">
                                    <div class="license-item__price-count"><span><span class="color-warning">$</span>{{$licence->price}}</span>
                                    </div>
                                </div>

                                <p class="license-item__subtitle">{!! __('Duration') !!} — <span class="color-warning">{{$licence->duration}} {!! __('days') !!}</span>
                                </p>

                                <ul class="license-item__list">
                                    <li>{!! __('Price') !!} -  <strong> {{$licence->price*rate('USD', $licence->currency->code)}} {{$licence->currency->code}}  </strong>
                                    </li>
{{--                                    <li><strong> {{$licence->price*rate('USD', 'PZM')}} {{'PZM'}}  </strong>--}}
{{--                                    </li>--}}
                                    <li>{!! __('Minimum car price') !!} - <strong>${{$licence->deposit_min}}</strong></li>
                                    <li>{!! __('Minimum motorcycle price') !!} - <strong>${{$licence->moto_min}}</strong></li>
                                    <li>{!! __('The most affordable price for a car or motorcycle') !!} - <strong>${{$licence->deposit_max}}</strong></li>
                                    <li>{!! __('Maximum purchase amount FST') !!} <strong>${{$licence->buy_amount}}</strong>
                                    </li>
                                    <li>{!! __('Maximum FST Sale Amount') !!} <strong>${{$licence->sell_amount}}</strong>
                                    </li>
                                </ul>
                                                            <div class="license-item__desc">
                                                                <p>{!! __('Car or moto price') !!} ${{$licence->deposit_max}}</p>
                                                                <p><small>({!! __('max') !!})</small></p>
                                                            </div>
                                <div class="license-item__buttons">
                                    <button type="button" class="btn js-modal" data-modal="licences{{$licence->id}}">{{__('Activate')}}
                                    </button>

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="slider-dots">   </div>
            </div>
        </div>
    </section>
{{--@endif--}}

@endsection
@section('modal_content')
    @foreach ($licences as $licence)
        <div class="modal" id="licences{{$licence->id}}">
            <div class="modal-inner modal-inner--dark">
                <div class="modal-inner__close js-close-modal">
                    <svg class="svg-icon">
                        <use href="/assets/icons/sprite.svg#icon-cross"></use>
                    </svg>
                </div>
                <div class="modal-header modal-header--border-bottom">
                    <h3 class="modal-title"><span>{{__('Are you sure?')}} </span>
                    </h3>
                </div>
                <form action="{{route('profile.buy_licence')}}" method="POST">
                    {{csrf_field()}}

                <div class="modal-content">

                    <div class="">
                        @if (user()->activeLicence())
                            <p style="color:white">{{__('Your current license')}} "{{user()->licence->name}}". {{__('When buying a license')}} "{{$licence->name}}" {{__('a valid license will become invalid')}}</p>
                        @endif


                            <p>{{$licence->name}} {!! __('(price - $'.number_format($licence->price).', validity-'.$licence->duration.' days).') !!}</p>
                            <p>{{ __('Maximum purchase amount FST')}} -  {{number_format($licence->buy_amount)}}$ {{ __('Maximum FST Sale Amount')}} - {{number_format($licence->sell_amount)}}$. </p>
                    </div>
{{--                    <input type="hidden" value="{{getUserWallet($licence->currency->code)->id}}"  name="wallet_id">--}}
                    <div class="balance-form__field">
                        <label>{{__('Currency')}}:</label>
                        <label class="select">--}}
                            <select id="wallet_id" name="wallet_id">

                                <option value="{{getUserWallet($licence->currency->code)->id}}">{{$licence->currency->code}} {{number_format($licence->price*rate('USD', $licence->currency->code), 6)}}</option>
{{--                                <option value="{{getUserWallet('PZM')->id}}">PZM {{number_format($licence->price*rate('USD', 'PZM'), 2)}}</option>--}}

                            </select>

                      </label>
                    </div>
                </div>
                <div class="modal-footer modal-footer--center">


                    <button class="btn btn--green btn--size-lg"><span>{{__('Yes')}}</span></button>
                    <input type="hidden" name="licence_id" value="{{$licence->id}}">
{{--                        <input type="hidden" name="wallet_id" value="{{getUserWallet('WEC')->id}}">--}}

                    <button type="button" class="btn btn--warning btn--size-lg js-close-modal"><span>{{__('No')}}</span>
                    </button>
                </div>
                </form>
            </div>
        </div>
    @endforeach

@endsection