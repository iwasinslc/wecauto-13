@extends('layouts.profile')
@section('title', __('Top up balance'))
@section('content')



    <section class="section-tabs">
        <div class="container">
            @include('partials.profile.finance_menu')
            <div class="section-tabs__content">
                <form class="tabs-form" method="POST" action="{{ route('profile.topup') }}">
                    {{ csrf_field() }}
                    <div class="tabs-form__row">
                        <div class="tabs-form__col-12">
                            <div class="field">
                                <label>{{__('Amount')}}</label>
                                <input id="topup_amount_modal" type="text" value="0.1" name="amount" required>
                            </div>
                        </div>
                    </div>
                    <div class="tabs-form__row">
                        <div class="tabs-form__col-6">
                            <div class="field">
                                <label>{{__('System')}}</label>
                                <label class="select">
                                    <select id="topup_ps_list" name="currency">


{{--                                        @foreach ($wec_ps as $payment_system)--}}
{{--                                            @foreach ($payment_system->currencies as $currency)--}}
{{--                                                --}}{{--                                        @if ($currency->code!=='USD')--}}

{{--                                                <option value="{{$currency->id}}:{{$payment_system->id}}">({{__('In')}}--}}
{{--                                                    WEC) - {{$payment_system->name}} {{$currency->code}}</option>--}}


{{--                                            @endforeach--}}


{{--                                        @endforeach--}}





                                            @foreach ($usd_ps as $payment_system)



                                                @foreach ($payment_system->currencies as $currency)
                                                    @if ($currency->code!=='BTC')
                                                        @if ($payment_system->code!=='free-kassa'&&$payment_system->code!=='payeer')
                                                            <option value="{{$currency->id}}:{{$payment_system->id}}">
                                                                ({{__('In')}} USD)
                                                                - {{$payment_system->name}} {{$currency->code}}</option>
                                                        @endif
                                                    @endif

                                                @endforeach


                                        @endforeach







                                        @foreach ($acc_ps as $payment_system)
                                            @foreach ($payment_system->currencies as $currency)
                                                @if ($currency->code=='ACC')
                                                    <option value="{{$currency->id}}:{{$payment_system->id}}">
                                                        ({{__('In')}} ACC)
                                                        - {{$payment_system->name}} {{$currency->code}}</option>
                                                @endif




                                            @endforeach


                                        @endforeach

{{--                                        @foreach ($pzm_ps as $payment_system)--}}
{{--                                            @foreach ($payment_system->currencies as $currency)--}}


{{--                                                <option value="{{$currency->id}}:{{$payment_system->id}}">({{__('In')}} PZM) - {{$payment_system->name}} {{$currency->code}}</option>--}}


{{--                                            @endforeach--}}


{{--                                        @endforeach--}}


                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tabs-form__bottom">
                        <button class="btn btn--warning btn--size-md">{{__('Top up')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>


    @include('partials.profile.finance_filter')

    <!-- /.card -->
@endsection
